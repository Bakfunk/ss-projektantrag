

<div class="top-bar">
    <div class="top-bar-left">
      <ul class="vertical medium-horizontal menu">
        <% loop $Menu(1) %>
        <li class="$LinkingMode"><a href="$Link" title="$Title.XML">$MenuTitle.XML</a></li>
        <% end_loop %>
           
      </ul>
    </div>
  </div>