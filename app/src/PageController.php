<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\Forms\EmailField;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\Form;
    use SilverStripe\Forms\FormAction;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\TextareaField;

    class PageController extends ContentController
    {
        /**
         * An array of actions that can be accessed via a request. Each array element should be an action name, and the
         * permissions or conditions required to allow the user to access it.
         *
         * <code>
         * [
         *     'action', // anyone can access this action
         *     'action' => true, // same as above
         *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
         *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
         * ];
         * </code>
         *
         * @var array
         */
        private static $allowed_actions = [
            'ContactForm'
        ];

        protected function init()
        {
            parent::init();
            // You can include any CSS or JS required by your project here.
            // See: https://docs.silverstripe.org/en/developer_guides/templates/requirements/
        }



        // Generierung eines Kontaktformulars
        public function ContactForm()
        {
            $fields = new FieldList(
                TextField::create('Name'),
                EmailField::create('Email'),
                TextField::create('Betreff'),
                TextareaField::create('Nachricht')
            );

            $actions = new FieldList(
                FormAction::create('doSubmitForm', 'Senden')
            );

            $validator = new SilverStripe\Forms\RequiredFields([
                'Name',
                'Email',
                'Betreff',
                'Nachricht'
            ]);
            
            $form = new Form($controller,'ContactForm', $fields, $actions, $validator);

            return $form;
        }


        public function SubmitContactForm($data, $form)
        {
            echo $data['Name'];
            echo $data['Email'];
     
            // echo $form->Fields()->dataFieldByName('Email')->Value();

            // Using the Form instance you can get / set status such as error messages.
            $form->sessionMessage('Successful!', 'good');
    
            // After dealing with the data you can redirect the user back.
            return $this->redirectBack();

        }

    }

}
